import ballerinax/kafka;
kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);
public function main() returns error? {
    string message = "Hello World, Peter";
    check kafkaProducer->send({
                                topic: "My-FirstKafka-topic",
                                value: message.toBytes() });

    check kafkaProducer->'flush();
}