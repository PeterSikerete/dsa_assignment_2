import ballerinax/kafka;
import ballerina/log;
kafka:ConsumerConfiguration consumerConfigs = {
    concurrentConsumers: 2,

    groupId: "group-id",
    topics: ["test-kafka-topic"],

    pollingInterval: 1
};
listener kafka:Listener kafkaListener =
            new (kafka:DEFAULT_URL, consumerConfigs);
service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,
                        kafka:ConsumerRecord[] records) returns error? {

        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }

    }
}
function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] messageContent = kafkaRecord.value;
    string message = check string:fromBytes(messageContent);

    log:printInfo("Received Message: " + message);
}